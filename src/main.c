//
//	Spherical POV Display LED Driver by Tim Kamenik
//

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <linux/spi/spidev.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>

 
#define USECS_PER_SEC 1000000
 
#define SPI_CHANNEL 0
#define CHANNELS_PER_LED 4

#define INTERRUPT_PIN 7

#define VERTICAL_RES 112
#define HORIZONTAL_RES 100

#define BUFFER_LEN VERTICAL_RES * CHANNELS_PER_LED

#define HORIZONTAL_SHIFT_TIME 200000

#define SHMEM_KEY 30419

// adapted from https://github.com/adafruit/Adafruit_DotStar_Pi/blob/master/dotstar.c 
struct spi_ioc_transfer xfer[3] = {
 { .tx_buf        = 0, // Header (zeros)
   .rx_buf        = 0,
   .len           = 4,
   .delay_usecs   = 0,
   .bits_per_word = 8,
   .cs_change     = 0 },
 { .rx_buf        = 0, // Color payload
   .delay_usecs   = 0,
   .bits_per_word = 8,
   .cs_change     = 0 },
 { .tx_buf        = 0, // Footer (zeros)
   .rx_buf        = 0,
   .delay_usecs   = 0,
   .bits_per_word = 8,
   .cs_change     = 0 }
};

int fd_spi;

const uint32_t buffer_len = VERTICAL_RES * CHANNELS_PER_LED;
uint32_t bitrate = 250000000 / 8;

uint8_t* shmem_buffer;

uint32_t horizontal_shift = 0;

struct timeval timestamp_last_turn_start; // updated by interrupt
struct timeval timestamp_last_interrupt; // updated by interrupt
struct timeval timestamp_last_horizontal_shift; // updated by interrupt

uint8_t do_horizontal_shift = 1;
uint8_t active_buffer_id = 0;

suseconds_t turn_period_us = 1000 * 1000 / 15;
suseconds_t step_period_us = 1000 * 1000 / 15 / 120;

// SETUP functions
int setup();
void setup_spi();
void setup_scheduler();
void setup_shmem();
void setup_interrupt();

// INTERRUPT functions
void turn_start_interrupt();

// MAIN fcuntions
float calculate_position_fraction();
void process_step(int step_number);
void perform_horizontal_shift();
void raw_write(uint8_t *ptr, size_t len);
struct timeval sleep_until_next_step(struct timeval timestamp_last_step);

// TOOLS functions
struct timeval get_current_timestamp();
int get_status_bit(int bit_id);


int main()
{
	struct timeval timestamp_next_step = get_current_timestamp();
	timestamp_last_horizontal_shift = get_current_timestamp();
	
	printf("Start\n");
	if(setup() == 0){
		printf("Setup done\n");
		
		while (1)
		{
			int step_number = (uint32_t) (calculate_position_fraction() * HORIZONTAL_RES + horizontal_shift) % HORIZONTAL_RES;
			// printf("Step Number: %d\n", step_number);
			if (step_number >= 0)
				process_step(HORIZONTAL_RES - step_number);
				
			timestamp_next_step = sleep_until_next_step(timestamp_next_step);
			
			if(do_horizontal_shift == 1)
			{
				perform_horizontal_shift();
			} else
			{
				horizontal_shift = 0;
			}
		}
	}
}

int setup()
{	
  setup_spi();
	
	setup_scheduler();
	
	setup_shmem();
	
	setup_interrupt();
	
	return 0;
}

void setup_interrupt()
{
	wiringPiSetup();
  pinMode(INTERRUPT_PIN, OUTPUT);
  wiringPiISR(INTERRUPT_PIN, INT_EDGE_FALLING, &turn_start_interrupt);	
}

void setup_spi()
{
	if((fd_spi = open("/dev/spidev0.0", O_RDWR)) < 0)
	{
		puts("Can't open /dev/spidev0.0 (try 'sudo')");
		exit(1);
	}
		
	uint8_t mode = SPI_MODE_0 | SPI_NO_CS;
	int ret = ioctl(fd_spi, SPI_IOC_WR_MODE, &mode);
	printf("Mode Setup: %d\n", ret);
	
	ret = ioctl(fd_spi, SPI_IOC_WR_MAX_SPEED_HZ, &bitrate);
	printf("Clock Setup: %d\n", ret);
}


//DEADLINE SCHED?
void setup_scheduler()
{
	// from http://www.isy.liu.se/edu/kurs/TSEA81/lecture_linux_realtime.html
	
	// Set our thread to real time priority
	struct sched_param sp;
	sp.sched_priority = 20; // Must be > 0. Threads with a higher
	// value in the priority field will be schedueld before
	// threads with a lower number.  Linux supports up to 99, but
        // the POSIX standard only guarantees up to 32 different
	// priority levels.

	// Note: If you are not using pthreads, use the
	// sched_setscheduler call instead.
	if(pthread_setschedparam(pthread_self(), SCHED_FIFO, &sp)){
		fprintf(stderr,"WARNING: Failed to set driver thread"
			"to real-time priority\n");
			exit(1);
	}
}

void setup_shmem()
{
	// from http://users.cs.cf.ac.uk/Dave.Marshall/C/node27.html
	
	int shmid;
	int size = 22 * 4096;
	key_t key = SHMEM_KEY;
	
	if ((shmid = shmget(key, size, 0666)) < 0) {
		perror("shmget");
		exit(1);
	}
	
	if ((shmem_buffer = shmat(shmid, NULL, 0)) == (uint8_t *) -1) {
    perror("shmat");
    exit(1);
  }
}

void turn_start_interrupt()
{	
	struct timeval difference;
	struct timeval timestamp_now = get_current_timestamp();
	timersub(&timestamp_now, &timestamp_last_interrupt, &difference);
	
	if(difference.tv_usec < 60000)
	{
		// printf("Interrupt blocked!\n");
		return;
	}
	timestamp_last_interrupt = get_current_timestamp();
	
	active_buffer_id = get_status_bit(0);
	do_horizontal_shift = get_status_bit(1);
	
  struct timeval timestamp_second_last_turn_start = timestamp_last_turn_start;
  timestamp_last_turn_start = timestamp_now;

  timersub(&timestamp_last_turn_start, &timestamp_second_last_turn_start, &difference);
  
	//turn_period_us = difference.tv_usec;
  turn_period_us = 0.75 * turn_period_us + 0.25 * difference.tv_usec;
  step_period_us = turn_period_us / HORIZONTAL_RES;
  printf("Turn frequency: %f\n", USECS_PER_SEC/((float)turn_period_us));
  fflush(stdout);
}

inline void process_step(int step_number)
{
  if (step_number < 0 || step_number >= HORIZONTAL_RES)
    return;

	if(active_buffer_id == 0)
		raw_write(shmem_buffer + 1 + step_number * buffer_len, buffer_len);
	if(active_buffer_id == 1)
		raw_write(shmem_buffer + 1 + buffer_len * HORIZONTAL_RES + step_number * buffer_len, buffer_len);
}

void perform_horizontal_shift()
{
	struct timeval timestamp_now = get_current_timestamp();
	struct timeval difference;
	timersub(&timestamp_now, &timestamp_last_horizontal_shift, &difference);
	
	if(difference.tv_usec > HORIZONTAL_SHIFT_TIME)
	{
		horizontal_shift += 1;
		timestamp_last_horizontal_shift = get_current_timestamp();
	}
}

inline float calculate_position_fraction()
{
  struct timeval timestamp_now = get_current_timestamp();
  struct timeval difference;
  timersub(&timestamp_now, &timestamp_last_turn_start, &difference);
  
  if (difference.tv_usec > turn_period_us)
    return -1;
  else
    return (float) difference.tv_usec / turn_period_us;
    // TODO: add epsilon?
}

inline struct timeval get_current_timestamp()
{
  struct timeval timestamp;
  gettimeofday(&timestamp, NULL);
  return timestamp;
}

inline struct timeval sleep_until_next_step(struct timeval timestamp_next_step)
{
  struct timeval timestamp_now = get_current_timestamp();
  struct timeval difference;
  timersub(&timestamp_next_step, &timestamp_now, &difference);
  
  if ((timestamp_now.tv_usec > timestamp_next_step.tv_usec && timestamp_now.tv_sec == timestamp_next_step.tv_sec) || timestamp_now.tv_sec > timestamp_next_step.tv_sec)
    // fprintf(stderr, "Missed step by over a second\n");
		;
  else
  {
    usleep(difference.tv_usec);
		//delayMicroseconds(difference.tv_usec);
    
    timestamp_now = get_current_timestamp();
    suseconds_t difference_us = (timestamp_next_step.tv_usec > timestamp_now.tv_usec) ? (timestamp_next_step.tv_usec - timestamp_now.tv_usec) : (timestamp_now.tv_usec - timestamp_next_step.tv_usec);
    // printf("usleep was off by %ld us\n", difference_us);
  }
  
  struct timeval timestamp_next_next_step;
  struct timeval delta;
  delta.tv_sec = 0;
  delta.tv_usec = step_period_us;
  timeradd(&timestamp_next_step, &delta, &timestamp_next_next_step);
  return timestamp_next_next_step;
}

inline void raw_write(uint8_t *ptr, uint32_t len)
{
	if(fd_spi >= 0) { // Hardware SPI
		xfer[0].speed_hz = bitrate;
		xfer[1].speed_hz = bitrate;
		xfer[2].speed_hz = bitrate;
		xfer[1].tx_buf   = (unsigned long)ptr;
		xfer[1].len      = len;
		
		if(VERTICAL_RES)
			xfer[2].len = (VERTICAL_RES + 15) / 16;
		else
			xfer[2].len = ((len / 4) + 15) / 16;
	
		ioctl(fd_spi, SPI_IOC_MESSAGE(3), xfer);
	}
}

inline int get_status_bit(int bit_id)
{
	return (*shmem_buffer >> bit_id) & 1;
}
