# Spherical POV Display LED Driver

## General
This is the LED Driver program for the Spherical POV Display. It measures the time per revolution from an optical sensor, reads image data from shared memory and writes the LED contents over the SPI. It runs on a RaspberryPi Zero W.

## Requirements
The program requires the WiringPi Library (http://wiringpi.com/) to access the RaspberryPis GPIOs.

## Compilation
Compiled using gcc with parameters:
gcc -O3 -lwiringPi -std=gnu99 -o main main.c

## Circuit
![](img/Rotor_circuit.png)